FROM hassanamin994/node_ffmpeg:6

WORKDIR /whatsapp-message-webhook

ENV GOOGLE_APPLICATION_CREDENTIALS=/whatsapp-message-webhook/gsc_creds.json

COPY . .
RUN npm install

CMD ["npm", "run", "docker:prod"]