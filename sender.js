const amqp = require('amqplib/callback_api')
require('dotenv').config()
const { sendTextMessage, sendMediaMessage } = require('vw-turn.io')
const queue = 'whatsapp_bot_message_queue'
const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER

amqp.connect(RABBITMQ_SERVER, (error0, connection) => {
  if (error0) {
    throw error0
  }
  connection.createChannel((error1, channel) => {
    if (error1) {
      throw error1
    }
    channel.prefetch(1)
    channel.assertQueue(queue, {
      durable: true
    })
    channel.consume(
      queue,
      msg => {
        const response = JSON.parse(msg.content.toString())
        if (response.videoUrl) {
          sendMediaMessage(response.videoUrl, response.to)
            .then(_ => channel.ack(msg))
            .catch(error => {
              channel.ack(msg)
              console.log(error)
            })
        } else {
          sendTextMessage(response.textMsg, response.to)
            .then(_ => channel.ack(msg))
            .catch(error => {
              channel.ack(msg)
              console.log(error)
            })
        }
      },
      {
        noAck: false
      }
    )
  })
})
