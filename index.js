require('./sender')
const express = require('express')
const amqp = require('amqplib/callback_api')
const mongoose = require('mongoose')
const { processMessage } = require('./messageDelegator')
require('dotenv').config()
const { login } = require('vw-turn.io')
const app = express()
const port = process.env.PORT || 4000
const queue = 'whatsapp_bot_message_queue'
const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER
const DB_CONNECTION_URL = process.env.WHATSAPP_MESSAGE_WEBHOOK_DATABASE_URL
const username = process.env.TURNIO_USERNAME
const password = process.env.TURNIO_PASSWORD

mongoose
  .connect(DB_CONNECTION_URL)
  .then(console.log('connected'))
  .catch(() => {
    console.log(err);
    process.exit(1);
  })

app.use(express.json())

amqp.connect(RABBITMQ_SERVER, (error0, connection) => {
  if (error0) {
    throw error0
  }
  connection.createChannel((error1, channel) => {
    if (error1) {
      throw error1
    }
    channel.assertQueue(queue, {
      durable: true
    })
    login(username, password, 'agent')
      .then(_ => {
        app.get('/', (req, res) => {
          res.status(200).send('OK')
        })

        app.post('/messages', (req, res) => {
          res.send('OK')
          if (!req.body.messages) return
          const messageType = req.body.messages[0].type
          const messageSender = req.body.messages[0].from
          const msg = {
            type: messageType,
            sender: messageSender
          }
          if (messageType === 'text') {
            msg.content = req.body.messages[0].text.body
          } else if (messageType === 'voice') {
            msg.content = req.body.messages[0].voice.id
          }
          processMessage(msg)
            .then(response => {
              response.textMessages.forEach(textMsg => {
                channel.sendToQueue(
                  queue,
                  Buffer.from(JSON.stringify({ textMsg, to: response.to })),
                  {
                    persistent: true
                  }
                )
              })
              if (response.videoUrl) {
                channel.sendToQueue(
                  queue,
                  Buffer.from(
                    JSON.stringify({
                      videoUrl: response.videoUrl,
                      to: response.to
                    })
                  ),
                  {
                    persistent: true
                  }
                )
              }
            })
            .catch(console.error)
        })
      })
      .catch(console.error)
  })
})

// login(process.env.TURNIO_USERNAME, process.env.TURNIO_PASSWORD)
//   .then(token => {
//     console.log(token)
//     request
//       .put(`https://whatsapp.turn.io/v1/messages/${req.body.messages[0].id}`)
//       .send({ status: 'read' })
//       .set('Authorization', `Bearer ${token}`)
//       .set('Content-Type', 'application/json')
//       .then(response => console.log(response))
//       .catch(error => console.log(error))
//   })
//   .catch(error => console.log(error))

app.listen(port, () => console.log(`app listening on port ${port}`))
