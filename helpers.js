const { isoLangs } = require('./languages')
const { Translate } = require('@google-cloud/translate').v2
const path = require('path')
const fs = require('fs')
const uuid = require('uuid').v4
const request = require('superagent')
const musicMetadata = require('music-metadata')
const { exec } = require('child_process')
const translate = new Translate()

const getLangInEnglish = langName => {
  return new Promise((resolve, reject) => {
    translate
      .translate(langName, 'en')
      .then(translation => {
        resolve(translation[0])
      })
      .catch(error => reject(error))
  })
}

const langCode = lang => {
  const langName =
    lang
      .toLowerCase()
      .charAt(0)
      .toUpperCase() + lang.toLowerCase().slice(1)

  for (let code in isoLangs) {
    if (langName == isoLangs[code].name) {
      return code
    }
  }
}

const getFileDrurationFromBuffer = buffer => {
  return new Promise((resolve, reject) => {
    fs.writeFile('voice.ogg', buffer, err => {
      if (err) {
        fs.unlink('voice.ogg', err => {
          console.log(err)
        })
        reject(err)
      } else {
        const cmd =
          'ffprobe -i voice.ogg -show_entries format=duration -v quiet -of csv="p=0"'
        exec(cmd, (error, stdout, stderr) => {
          fs.unlink('voice.ogg', err => {
            if (err) console.log(err)
          })
          if (error) console.error(`exec error: ${error}`)
          if (stderr) console.error(`stderr: ${stderr}`)
          response = JSON.parse(stdout)
          if (response) {
            resolve(response)
          }
          reject(error)
        })
      }
    })
  })
}

const getFileDuration = url => {
  return new Promise((resolve, reject) => {
    const filePath = url.split('/').pop()

    downloadFile(url, filePath)
      .then(() => {
        return musicMetadata.parseFile(filePath)
      })
      .then(md => {
        fs.unlink(filePath, e => {
          if (e) console.log('removed file', e)
        })
        return resolve(md.format.duration)
      })
      .catch(reject)
  })
}

const downloadFile = (url, targetPath) => {
  return new Promise((resolve, reject) => {
    const extension = url.split('.').pop()
    const filePath =
      targetPath || path.join(__dirname, `${uuid()}.${extension}`)
    const stream = fs.createWriteStream(filePath)
    stream.on('finish', () => resolve(filePath))
    stream.on('error', err => reject(err))
    // Start download
    request.get(url).pipe(stream)
  })
}

module.exports = {
  getLangInEnglish,
  langCode,
  getFileDuration,
  getFileDrurationFromBuffer
}
