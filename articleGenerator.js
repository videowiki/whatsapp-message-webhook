const request = require('superagent')

const generateArticle = langFrom => {
  return new Promise((resolve, reject) => {
    request
      .get(
        `https://api.videowiki.org/api/article/whatsapp-bot?langFrom=${langFrom}`
      )
      .set('Content-Type', 'application/json')
      .then(({ body: { article } }) => resolve(article))
      .catch(error => reject(error))
  })
}

const formatSlides = article => {
  let originalSlides = []
  let number = 1
  article.slides.forEach(slide => {
    slide.content.forEach(subslide => {
      if (subslide.speakerProfile.speakerNumber != -1) {
        originalSlides.push({
          text: subslide.text,
          audio: subslide.audio,
          video: subslide.media[0].url,
          slidePosition: slide.position,
          subslideposition: subslide.position,
          number: number++
        })
      }
    })
  })
  let translatedSlides = JSON.parse(JSON.stringify(originalSlides))
  translatedSlides.forEach(slide => {
    delete slide.text
    delete slide.audio
  })
  return { originalSlides, translatedSlides }
}

module.exports = { generateArticle, formatSlides }
