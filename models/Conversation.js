const { Schema } = require('mongoose')

const STAGE_ENUM = [
  'option_select',
  'lang_from',
  'lang_to',
  'text_translation',
  'audio_translation',
  'discarded',
  'completed'
]

const EDITING_STAGE_ENUM = ['pending', 'editing_request', 'editing_slide']

const TRANSLATION_TYPE_ENUM = ['text', 'audio', 'text_audio']

const SlideSchema = new Schema({
  text: { type: String },
  number: { type: Number },
  audio: { type: String },
  video: { type: String },
  slidePosition: { type: Number },
  subslidePosition: { type: Number }
})

const ConversationSchema = new Schema({
  articleId: { type: String },
  locked: { type: Boolean },
  stage: { type: String, enum: STAGE_ENUM },
  originalSlides: [SlideSchema],
  translatedSlides: [SlideSchema],
  currentSlideNumber: { type: Number },
  translationType: { type: String, enum: TRANSLATION_TYPE_ENUM },
  textTranslated: { type: Boolean, default: false },
  audioTranslated: { type: Boolean, default: false },
  langFrom: { type: String },
  langTo: { type: String },
  contactNumber: { type: String },
  editing: { type: Boolean, default: false },
  editingStage: { type: String, enum: EDITING_STAGE_ENUM, default: 'pending' },
  editingSlideNumber: { type: Number }
})

module.exports = {
  ConversationSchema
}
