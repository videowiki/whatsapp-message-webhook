const { getMedia } = require('vw-turn.io')
const mongoose = require('mongoose')
const { ConversationSchema } = require('./models/Conversation')
const Conversation = mongoose.model('Conversation', ConversationSchema)
const { generateArticle, formatSlides } = require('./articleGenerator')
const { isoLangs } = require('./languages')
const {
  getLangInEnglish,
  langCode,
  getFileDrurationFromBuffer,
  getFileDuration
} = require('./helpers')

const conversationMessages = {
  joinCode: 'join videowiki',
  invalidCode: 'The join code is invalid',
  langFromQ: 'Which language do you want to translate from (Source Language)?',
  langToQ: 'Which language do you want to translate in (Target Language)?',
  invalidLang: 'Please send a valid language!',
  invalidText: 'Please use text message to translate this text!',
  invalidVoice: 'Please use voice message to translate this voice!',
  mainMenu:
    'Hi, Welcome to *VideoWiki!* What do you want to do today?\n\n*1.* Proofread - Break Videos into small slides\n\n*2.* Proofread - Listen to Audio and type the text\n\n*3.* Translate text 💬\n\n*4.* Add voice-overs 🎙\n\n*5.* Add subtitles to the videos\n\n*6.* Translate text and add voice-overs 💬🎙\n\n👉 Send any of the number *"1,2,3,4,5,6"* to choose your option\n\n👉 Send *"0"* to get back to this menu.',
  startTextTranslation: 'Lets start text translation',
  startAudioTranslation: 'Lets start audio translation',
  noArticle:
    'There is no articles available for this language please select anothe one',
  menuAndMistake:
    'Type *Menu* to see the Main Menu\n\nType *Mistake* to redo a slide again',
  editing: 'Please send the number of the slide you want to edit',
  invalidEditing: 'Please send a valid slide number to edit!',
  longVoice:
    'The translation voice must not be greater than the original voice',
  voiceTip:
    '💡 *Tip*: The translation voice must not be greater than the original voice',
  success:
    'Thank you, the video is translated successfully!\n\nType *Start* to start a new translation',
  invalidInput: 'Invalid input',
  confirmText: 'Type *Confirm* to confirm the text translation',
  confirmAudio: 'Type *Confirm* to confirm the audio translation'
}

const ACTION_CODES = {
  SHOW_MENU: ['0', 'menu'],
  PROOFREAD_BREAK_VIDEOS: ['1'],
  PROOFREAD_TYPE_TEXT: ['2'],
  SELECT_TEXT_TRANSLATION: ['3'],
  SELECT_AUDIO_TRANSLATION: ['4'],
  ADD_SUBTITLES: ['5'],
  SELECT_TEXT_AND_AUDIO_TRANSLATION: ['6'],
  CONFIRM: ['confirm'],
  MISTAKE: ['mistake'],
  START: ['start']
}

function isAction (text, action) {
  return action.some(a => a === text.toLowerCase())
}

function processMessage (msg) {
  return new Promise((resolve, reject) => {
    Conversation.find({ contactNumber: msg.sender })
      .then(conversations => {
        if (conversations.length) {
          Conversation.findOne({
            contactNumber: msg.sender
          })
            .nor([{ stage: 'completed' }, { stage: 'discarded' }])
            .then(conversation => {
              if (conversation) {
                handleConversationStages(conversation, msg)
                  .then(resolve)
                  .catch(reject)
              } else if (isAction(msg.content, ACTION_CODES.START)) {
                startNewConversation(msg)
                  .then(resolve)
                  .catch(reject)
              } else {
                resolve({
                  textMessages: [conversationMessages.invalidInput],
                  to: msg.sender
                })
              }
            })
            .catch(error => reject(error))
        } else {
          startNewConversation(msg)
          .then(resolve)
          .catch(reject)
        }
      })
      .catch(error => reject(error))
  })
}

function startNewConversation (msg) {
  return new Promise((resolve, reject) => {
    Conversation.create({
      stage: 'option_select',
      contactNumber: msg.sender
    })
      .then(
        resolve({
          textMessages: [conversationMessages.mainMenu],
          to: msg.sender
        })
      )
      .catch(error => reject(error))
  })
}

function handleConversationStages (conversation, msg) {
  return new Promise((resolve, reject) => {
    if (conversation.stage === 'option_select') {
      handleOptionSelectStage(msg, conversation)
        .then(resolve)
        .catch(reject)
    } else if (conversation.stage === 'lang_from') {
      handleLangFromStage(msg, conversation)
        .then(resolve)
        .catch(reject)
    } else if (conversation.stage === 'lang_to') {
      handleLangToStage(msg, conversation)
        .then(resolve)
        .catch(reject)
    } else if (
      conversation.stage === 'text_translation' ||
      conversation.stage === 'audio_translation'
    ) {
      handleTranslationStage(msg, conversation, resolve, reject)
        .then(resolve)
        .catch(reject)
    }
  })
}

function handleOptionSelectStage (msg, conversation) {
  return new Promise((resolve, reject) => {
    if (isAction(msg.content, ACTION_CODES.SHOW_MENU)) {
      return resolve({
        textMessages: [conversationMessages.mainMenu],
        to: msg.sender
      })
    }
    if (isAction(msg.content, ACTION_CODES.PROOFREAD_BREAK_VIDEOS)) {
      return handleProofreadBreakVideos(msg, conversation)
        .then(resolve)
        .catch(reject)
    }
    if (isAction(msg.content, ACTION_CODES.PROOFREAD_TYPE_TEXT)) {
      return handleProofreadTypeText(msg, conversation)
        .then(resolve)
        .catch(reject)
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_TEXT_TRANSLATION)) {
      return handleTranslationSelect(msg, conversation, 'text')
        .then(resolve)
        .catch(reject)
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_AUDIO_TRANSLATION)) {
      return handleTranslationSelect(msg, conversation, 'audio')
        .then(resolve)
        .catch(reject)
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_TEXT_AND_AUDIO_TRANSLATION)) {
      return handleTranslationSelect(msg, conversation, 'text_audio')
        .then(resolve)
        .catch(reject)
    }
    if (isAction(msg.content, ACTION_CODES.ADD_SUBTITLES)) {
      return handleAddSubtitles(msg, conversation)
        .then(resolve)
        .catch(reject)
    }
    return resolve({
      textMessages: [conversationMessages.invalidInput],
      to: msg.sender
    })
  })
}

function handleLangFromStage (msg, conversation) {
  return new Promise((resolve, reject) => {
    const nativeLangCode = langCode(msg.content)
    if (nativeLangCode) {
      generateArticle(nativeLangCode)
        .then(article => {
          if (!article) {
            return resolve({
              textMessages: [conversationMessages.noArticle],
              to: msg.sender
            })
          }
          const { originalSlides, translatedSlides } = formatSlides(article)
          conversation.articleId = article._id
          conversation.langFrom = nativeLangCode
          conversation.stage = 'lang_to'
          conversation.originalSlides = originalSlides
          conversation.translatedSlides = translatedSlides
          conversation
            .save()
            .then(
              resolve({
                textMessages: [conversationMessages.langToQ],
                to: msg.sender
              })
            )
            .catch(error => reject(error))
        })
        .catch(error => reject(error))
    } else {
      getLangInEnglish(msg.content)
        .then(langName => {
          const code = langCode(langName)
          if (code) {
            generateArticle(code)
              .then(article => {
                if (!article) {
                  return resolve({
                    textMessages: [conversationMessages.noArticle],
                    to: msg.sender
                  })
                }
                const { originalSlides, translatedSlides } = formatSlides(
                  article
                )
                conversation.articleId = article._id
                conversation.langFrom = code
                conversation.stage = 'lang_to'
                conversation.originalSlides = originalSlides
                conversation.translatedSlides = translatedSlides
                conversation
                  .save()
                  .then(
                    resolve({
                      textMessages: [conversationMessages.langToQ],
                      to: msg.sender
                    })
                  )
                  .catch(error => reject(error))
              })
              .catch(error => reject(error))
          } else {
            resolve({
              textMessages: [conversationMessages.invalidLang],
              to: msg.sender
            })
          }
        })
        .catch(error => reject(error))
    }
  })
}

function handleLangToStage (msg, conversation) {
  return new Promise((resolve, reject) => {
    const nativeLangCode = langCode(msg.content)
    if (nativeLangCode) {
      conversation.langTo = nativeLangCode
      conversation
        .save()
        .then(() => {
          startTranslation(msg, conversation, conversation.translationType)
            .then(resolve)
            .catch(reject)
        })
        .catch(error => reject(error))
    } else {
      getLangInEnglish(msg.content)
        .then(translation => {
          const code = langCode(translation)
          if (code) {
            conversation.langTo = code
            conversation.stage = 'translation_type'
            conversation
              .save()
              .then(() => {
                startTranslation(
                  msg,
                  conversation,
                  conversation.translationType
                )
                  .then(resolve)
                  .catch(reject)
              })
              .catch(error => reject(error))
          } else {
            resolve({
              textMessages: [conversationMessages.invalidLang],
              to: msg.sender
            })
          }
        })
        .catch(error => reject(error))
    }
  })
}

function handleTranslationStage (msg, conversation) {
  return new Promise((resolve, reject) => {
    if (conversation.editing) {
      handleEditTranslation(conversation, msg)
        .then(resolve)
        .catch(reject)
    } else if (
      isAction(msg.content, ACTION_CODES.CONFIRM) &&
      conversation.currentSlideNumber === 0 &&
      conversation.stage === 'text_translation' &&
      conversation.textTranslated
    ) {
      handleEndOfTextTranslation(msg, conversation)
        .then(resolve)
        .catch(reject)
    } else if (
      isAction(msg.content, ACTION_CODES.CONFIRM) &&
      conversation.currentSlideNumber === 0 &&
      conversation.stage === 'audio_translation' &&
      conversation.audioTranslated
    ) {
      handleEndOfAudioTranslation(conversation, msg)
        .then(resolve)
        .catch(reject)
    } else if (
      isAction(msg.content, ACTION_CODES.MISTAKE) &&
      conversation.currentSlideNumber !== 1
    ) {
      handleMistake(conversation, msg)
        .then(resolve)
        .catch(reject)
    } else if (isAction(msg.content, ACTION_CODES.SHOW_MENU)) {
      resolve({
        textMessages: [conversationMessages.mainMenu],
        to: msg.sender
      })
    } else if (isAction(msg.content, ACTION_CODES.PROOFREAD_BREAK_VIDEOS)) {
      conversation
        .remove()
        .then(_ => {
          Conversation.create({
            stage: 'option_select',
            contactNumber: msg.sender
          })
            .then(resolve({ textMessages: ['Coming soon'], to: msg.sender }))
            .catch(error => reject(error))
        })
        .catch(error => reject(error))
    } else if (isAction(msg.content, ACTION_CODES.PROOFREAD_TYPE_TEXT)) {
      conversation
        .remove()
        .then(_ => {
          Conversation.create({
            stage: 'option_select',
            contactNumber: msg.sender
          })
            .then(resolve({ textMessages: ['Coming soon'], to: msg.sender }))
            .catch(error => reject(error))
        })
        .catch(error => reject(error))
    } else if (isAction(msg.content, ACTION_CODES.ADD_SUBTITLES)) {
      conversation
        .remove()
        .then(_ => {
          Conversation.create({
            stage: 'option_select',
            contactNumber: msg.sender
          })
            .then(resolve({ textMessages: ['Coming soon'], to: msg.sender }))
            .catch(error => reject(error))
        })
        .catch(error => reject(error))
    } else if (isAction(msg.content, ACTION_CODES.SELECT_TEXT_TRANSLATION)) {
      conversation
        .remove()
        .then(_ => {
          Conversation.create({
            stage: 'lang_from',
            translationType: 'text',
            contactNumber: msg.sender
          })
            .then(
              resolve({
                textMessages: [conversationMessages.langFromQ],
                to: msg.sender
              })
            )
            .catch(error => reject(error))
        })
        .catch(error => reject(error))
    } else if (isAction(msg.content, ACTION_CODES.SELECT_AUDIO_TRANSLATION)) {
      conversation
        .remove()
        .then(_ => {
          Conversation.create({
            stage: 'lang_from',
            translationType: 'audio',
            contactNumber: msg.sender
          })
            .then(
              resolve({
                textMessages: [conversationMessages.langFromQ],
                to: msg.sender
              })
            )
            .catch(error => reject(error))
        })
        .catch(error => reject(error))
    } else if (
      isAction(msg.content, ACTION_CODES.SELECT_TEXT_AND_AUDIO_TRANSLATION)
    ) {
      conversation
        .remove()
        .then(_ => {
          Conversation.create({
            stage: 'lang_from',
            translationType: 'text_audio',
            contactNumber: msg.sender
          })
            .then(
              resolve({
                textMessages: [conversationMessages.langFromQ],
                to: msg.sender
              })
            )
            .catch(error => reject(error))
        })
        .catch(error => reject(error))
    } else if (
      conversation.currentSlideNumber === 0 &&
      (conversation.textTranslated || conversation.audioTranslated)
    ) {
      resolve({
        textMessages: [conversationMessages.invalidInput],
        to: msg.sender
      })
    } else {
      if (conversation.stage === 'text_translation') {
        handleTextTranslationStage(msg, conversation)
          .then(resolve)
          .catch(reject)
      } else if (conversation.stage === 'audio_translation') {
        handleAudioTranslationStage(msg, conversation)
          .then(resolve)
          .catch(reject)
      }
    }
  })
}

function handleMistake (conversation, msg) {
  return new Promise((resolve, reject) => {
    conversation.editing = true
    conversation.editingStage = 'editing_request'
    conversation
      .save()
      .then(
        resolve({
          textMessages: [conversationMessages.editing],
          to: msg.sender
        })
      )
      .catch(error => reject(error))
  })
}

function handleEndOfAudioTranslation (conversation, msg) {
  return new Promise((resolve, reject) => {
    conversation.stage = 'completed'
    conversation
      .save()
      .then(
        resolve({
          textMessages: [conversationMessages.success],
          to: msg.sender
        })
      )
      .catch(error => reject(error))
  })
}

function startTranslation (msg, conversation, type) {
  return new Promise((resolve, reject) => {
    const translatedSlides = JSON.parse(
      JSON.stringify(conversation.originalSlides)
    )
    translatedSlides.forEach(slide => {
      delete slide.text
      delete slide.audio
    })
    conversation.translatedSlides = translatedSlides
    conversation.currentSlideNumber = 1
    conversation.textTranslated = false
    conversation.audioTranslated = false

    if (type === 'text') {
      // TEXT TYPE
      conversation.stage = 'text_translation'
      conversation.translationType = 'text'
      return conversation
        .save()
        .then(
          resolve({
            videoUrl: conversation.originalSlides[0].video,
            textMessages: [
              conversationMessages.startTextTranslation,
              `Slide 1/${conversation.originalSlides.length}\n\n${
                isoLangs[conversation.langFrom].name
              }:\n\n${conversation.originalSlides[0].text}`
            ],
            to: msg.sender
          })
        )
        .catch(error => reject(error))
    }

    if (type === 'audio') {
      // AUDIO_TYPE
      conversation.stage = 'audio_translation'
      conversation.translationType = 'audio'
      return conversation
        .save()
        .then(
          resolve({
            videoUrl: conversation.originalSlides[0].video,
            textMessages: [
              conversationMessages.startAudioTranslation,
              conversationMessages.voiceTip,
              `Slide 1/${conversation.originalSlides.length}\n\n${
                isoLangs[conversation.langFrom].name
              }:\n\n${conversation.originalSlides[0].text}`
            ],
            to: msg.sender
          })
        )
        .catch(error => reject(error))
    }

    if (type === 'text_audio') {
      // TEXT_AUDIO
      conversation.stage = 'text_translation'
      conversation.translationType = 'text_audio'
      return conversation
        .save()
        .then(
          resolve({
            videoUrl: conversation.originalSlides[0].video,
            textMessages: [
              conversationMessages.startTextTranslation,
              `Slide 1/${conversation.originalSlides.length}\n\n${
                isoLangs[conversation.langFrom].name
              }:\n\n${conversation.originalSlides[0].text}`
            ],
            to: msg.sender
          })
        )
        .catch(error => reject(error))
    }

    return resolve({
      textMessages: [conversationMessages.invalidInput],
      to: msg.sender
    })
  })
}

function handleEndOfTextTranslation (msg, conversation) {
  return new Promise((resolve, reject) => {
    if (
      conversation.translationType === 'audio' ||
      conversation.translationType === 'text_audio'
    ) {
      conversation.stage = 'audio_translation'
      conversation.currentSlideNumber = 1
      conversation
        .save()
        .then(updatedConversation => {
          const resolved = {
            videoUrl: updatedConversation.originalSlides[0].video,
            textMessages: [
              conversationMessages.startAudioTranslation,
              conversationMessages.voiceTip,
              `Slide 1/${updatedConversation.originalSlides.length}\n\n${
                isoLangs[updatedConversation.langFrom].name
              }:\n\n${updatedConversation.originalSlides[0].text}`
            ],
            to: msg.sender
          }
          if (updatedConversation.textTranslated) {
            resolved.textMessages[2] += `\n\n${
              isoLangs[updatedConversation.langTo].name
            }:\n\n${updatedConversation.translatedSlides[0].text}`
          }
          resolve(resolved)
        })
        .catch(error => reject(error))
    } else if (conversation.translationType === 'text') {
      conversation.stage = 'completed'
      conversation
        .save()
        .then(
          resolve({
            textMessages: [conversationMessages.success],
            to: msg.sender
          })
        )
        .catch(error => reject(error))
    }
  })
}

function handleEditTranslation (conversation, msg) {
  return new Promise((resolve, reject) => {
    if (conversation.editingStage === 'editing_request') {
      handleEditingRequest(msg, conversation)
        .then(resolve)
        .catch(reject)
    } else if (conversation.editingStage === 'editing_slide') {
      handleEditingSlide(msg, conversation)
        .then(resolve)
        .catch(reject)
    }
  })
}

function handleEditingSlide (msg, conversation) {
  return new Promise((resolve, reject) => {
    if (msg.type !== 'text' && conversation.stage === 'text_translation') {
      resolve({
        textMessages: [conversationMessages.invalidText],
        to: msg.sender
      })
    } else if (
      msg.type !== 'voice' &&
      conversation.stage === 'audio_translation'
    ) {
      resolve({
        textMessages: [conversationMessages.invalidVoice],
        to: msg.sender
      })
    } else {
      if (conversation.stage === 'audio_translation') {
        getFileDuration(
          conversation.originalSlides[conversation.editingSlideNumber - 1].audio
        )
          .then(originalDuration => {
            getMedia(msg.content)
              .then(buffer => {
                getFileDrurationFromBuffer(buffer)
                  .then(translationDuration => {
                    if (
                      Number(translationDuration.toFixed(1)) >
                      Number(originalDuration.toFixed(1))
                    ) {
                      resolve({
                        textMessages: [conversationMessages.longVoice],
                        to: msg.sender
                      })
                    } else {
                      conversation.editing = false
                      conversation.editingStage = 'pending'
                      conversation.translatedSlides[
                        conversation.editingSlideNumber - 1
                      ].audio = msg.content
                      conversation.editingSlideNumber = 0
                      conversation
                        .save()
                        .then(updatedConversation => {
                          if (updatedConversation.currentSlideNumber === 0) {
                            resolve({
                              textMessages: [
                                `${conversationMessages.menuAndMistake}\n\n${conversationMessages.confirmAudio}`
                              ],
                              to: msg.sender
                            })
                          } else {
                            const resolved = {
                              videoUrl:
                                updatedConversation.originalSlides[
                                  updatedConversation.currentSlideNumber - 1
                                ].video,
                              textMessages: [
                                conversationMessages.menuAndMistake,
                                `Slide ${
                                  updatedConversation.currentSlideNumber
                                }/${
                                  updatedConversation.originalSlides.length
                                }\n\n${
                                  isoLangs[updatedConversation.langFrom].name
                                }:\n\n${
                                  updatedConversation.originalSlides[
                                    updatedConversation.currentSlideNumber - 1
                                  ].text
                                }`
                              ],
                              to: msg.sender
                            }
                            if (updatedConversation.textTranslated) {
                              resolved.textMessages[1] += `\n\n${
                                isoLangs[updatedConversation.langTo].name
                              }:\n\n${
                                updatedConversation.translatedSlides[
                                  updatedConversation.currentSlideNumber - 1
                                ].text
                              }`
                            }
                            resolve(resolved)
                          }
                        })
                        .catch(error => reject(error))
                    }
                  })
                  .catch(error => reject(error))
              })
              .catch(error => reject(error))
          })
          .catch(error => reject(error))
      } else if (conversation.stage === 'text_translation') {
        conversation.editing = false
        conversation.editingStage = 'pending'
        conversation.translatedSlides[
          conversation.editingSlideNumber - 1
        ].text = msg.content
        conversation.editingSlideNumber = 0
        conversation
          .save()
          .then(updatedConversation => {
            if (updatedConversation.currentSlideNumber === 0) {
              resolve({
                textMessages: [
                  `${conversationMessages.menuAndMistake}\n\n${conversationMessages.confirmText}`
                ],
                to: msg.sender
              })
            } else {
              resolve({
                videoUrl:
                  updatedConversation.originalSlides[
                    updatedConversation.currentSlideNumber - 1
                  ].video,
                textMessages: [
                  conversationMessages.menuAndMistake,
                  `Slide ${updatedConversation.currentSlideNumber}/${
                    updatedConversation.originalSlides.length
                  }\n\n${isoLangs[updatedConversation.langFrom].name}:\n\n${
                    updatedConversation.originalSlides[
                      updatedConversation.currentSlideNumber - 1
                    ].text
                  }`
                ],
                to: msg.sender
              })
            }
          })
          .catch(error => reject(error))
      }
    }
  })
}

function handleEditingRequest (msg, conversation, resolve, reject) {
  return new Promise((resolve, reject) => {
    const slideNumber = Number(
      msg.content
        .replace('Slide ', '')
        .replace('slide ', '')
        .replace('.', '')
    )
    if (
      slideNumber > 0 &&
      slideNumber <= conversation.originalSlides.length &&
      ((conversation.stage === 'text_translation' &&
        conversation.translatedSlides[slideNumber - 1].text) ||
        (conversation.stage === 'audio_translation' &&
          conversation.translatedSlides[slideNumber - 1].audio))
    ) {
      conversation.editingStage = 'editing_slide'
      conversation.editingSlideNumber = slideNumber
      conversation
        .save()
        .then(updatedConversation => {
          if (updatedConversation.stage === 'text_translation') {
            resolve({
              videoUrl:
                updatedConversation.originalSlides[
                  updatedConversation.editingSlideNumber - 1
                ].video,
              textMessages: [
                `Slide ${slideNumber}/${
                  conversation.originalSlides.length
                }\n\n${isoLangs[updatedConversation.langFrom].name}:\n\n${
                  conversation.originalSlides[slideNumber - 1].text
                }\n\n${isoLangs[updatedConversation.langTo].name}:\n\n${
                  conversation.translatedSlides[slideNumber - 1].text
                }`
              ],
              to: msg.sender
            })
          } else if (updatedConversation.stage === 'audio_translation') {
            const resolved = {
              videoUrl:
                updatedConversation.originalSlides[slideNumber - 1].video,
              textMessages: [
                conversationMessages.voiceTip,
                `Slide ${slideNumber}/${
                  updatedConversation.originalSlides.length
                }\n\n${isoLangs[updatedConversation.langFrom].name}:\n\n${
                  updatedConversation.originalSlides[slideNumber - 1].text
                }`
              ],
              to: msg.sender
            }
            if (updatedConversation.textTranslated) {
              resolved.textMessages[1] += `\n\n${
                isoLangs[updatedConversation.langTo].name
              }:\n\n${
                updatedConversation.translatedSlides[slideNumber - 1].text
              }`
            }
            resolve(resolved)
          }
        })
        .catch(error => reject(error))
    } else {
      resolve({
        textMessages: [conversationMessages.invalidEditing],
        to: msg.sender
      })
    }
  })
}

function handleTextTranslationStage (msg, conversation) {
  return new Promise((resolve, reject) => {
    if (msg.type !== 'text') {
      resolve({
        textMessages: [conversationMessages.invalidText],
        to: msg.sender
      })
    } else {
      conversation.translatedSlides[conversation.currentSlideNumber - 1].text =
        msg.content
      if (
        conversation.currentSlideNumber >= conversation.originalSlides.length
      ) {
        conversation.textTranslated = true
        conversation.currentSlideNumber = 0
        conversation
          .save()
          .then(
            resolve({
              textMessages: [
                `${conversationMessages.menuAndMistake}\n\n${conversationMessages.confirmText}`
              ],
              to: msg.sender
            })
          )
          .catch(error => reject(error))
      } else {
        conversation.currentSlideNumber += 1
        conversation
          .save()
          .then(updatedConversation => {
            resolve({
              videoUrl:
                updatedConversation.originalSlides[
                  updatedConversation.currentSlideNumber - 1
                ].video,
              textMessages: [
                conversationMessages.menuAndMistake,
                `Slide ${updatedConversation.currentSlideNumber}/${
                  updatedConversation.originalSlides.length
                }\n\n${isoLangs[updatedConversation.langFrom].name}:\n\n${
                  updatedConversation.originalSlides[
                    updatedConversation.currentSlideNumber - 1
                  ].text
                }`
              ],
              to: msg.sender
            })
          })
          .catch(error => reject(error))
      }
    }
  })
}

function handleAudioTranslationStage (msg, conversation) {
  return new Promise((resolve, reject) => {
    if (msg.type !== 'voice') {
      resolve({
        textMessages: [conversationMessages.invalidVoice],
        to: msg.sender
      })
    } else {
      getFileDuration(
        conversation.originalSlides[conversation.currentSlideNumber - 1].audio
      )
        .then(originalDuration => {
          getMedia(msg.content)
            .then(buffer => {
              getFileDrurationFromBuffer(buffer)
                .then(translationDuration => {
                  if (
                    Number(translationDuration.toFixed(1)) >
                    Number(originalDuration.toFixed(1))
                  ) {
                    resolve({
                      textMessages: [conversationMessages.longVoice],
                      to: msg.sender
                    })
                  } else {
                    conversation.translatedSlides[
                      conversation.currentSlideNumber - 1
                    ].audio = msg.content
                    if (
                      conversation.currentSlideNumber >=
                      conversation.originalSlides.length
                    ) {
                      conversation.audioTranslated = true
                      conversation.currentSlideNumber = 0
                      conversation
                        .save()
                        .then(
                          resolve({
                            textMessages: [
                              `${conversationMessages.menuAndMistake}\n\n${conversationMessages.confirmAudio}`
                            ],
                            to: msg.sender
                          })
                        )
                        .catch(error => reject(error))
                    } else {
                      conversation.currentSlideNumber += 1
                      conversation
                        .save()
                        .then(updatedConversation => {
                          const resolved = {
                            videoUrl:
                              updatedConversation.originalSlides[
                                updatedConversation.currentSlideNumber - 1
                              ].video,
                            textMessages: [
                              conversationMessages.menuAndMistake,
                              conversationMessages.voiceTip,
                              `Slide ${
                                updatedConversation.currentSlideNumber
                              }/${
                                updatedConversation.originalSlides.length
                              }\n\n${
                                isoLangs[updatedConversation.langFrom].name
                              }:\n\n${
                                updatedConversation.originalSlides[
                                  updatedConversation.currentSlideNumber - 1
                                ].text
                              }`
                            ],
                            to: msg.sender
                          }
                          if (updatedConversation.textTranslated) {
                            resolved.textMessages[2] += `\n\n${
                              isoLangs[updatedConversation.langTo].name
                            }:\n\n${
                              updatedConversation.translatedSlides[
                                updatedConversation.currentSlideNumber - 1
                              ].text
                            }`
                          }
                          resolve(resolved)
                        })
                        .catch(error => reject(error))
                    }
                  }
                })
                .catch(error => reject(error))
            })
            .catch(error => reject(error))
        })
        .catch(error => reject(error))
    }
  })
}

function handleTranslationSelect (msg, conversation, translationType) {
  return new Promise((resolve, reject) => {
    conversation.stage = 'lang_from'
    conversation.translationType = translationType
    conversation
      .save()
      .then(() => {
        resolve({
          textMessages: [conversationMessages.langFromQ],
          to: msg.sender
        })
      })
      .catch(reject)
  })
}

function handleProofreadBreakVideos (msg, conversation) {
  return Promise.resolve({
    textMessages: ['Coming soon'],
    to: msg.sender
  })
}

function handleProofreadTypeText (msg, conversation) {
  return Promise.resolve({
    textMessages: ['Coming soon'],
    to: msg.sender
  })
}

function handleAddSubtitles (msg, conversation) {
  return Promise.resolve({
    textMessages: ['Coming soon'],
    to: msg.sender
  })
}

module.exports = { processMessage }
